/*
 * Пункт меню. Конкретное изделие.
 */
var React = require('react');
var Col = require('react-bootstrap/lib/Col');
var OverlayTrigger = require('react-bootstrap/lib/OverlayTrigger');
var Tooltip = require('react-bootstrap/lib/Tooltip');
var Image = require('react-bootstrap/lib/Image');

var ProductMixin = {
    handleSelect: function () {
        this.props.selectCallback(this.props.type);
    },
    getClassName: function () {
        return "products-menu-item " + (this.props.disabled ? "disabled" : "");
    },
    render: function () {
        var isSelected = this.props.selectedProductType == this.props.type;
        var form = null;
        if (isSelected) {
            form = this.render_form() || null;
        }
        var className = this.getClassName();
        var tooltip = <Tooltip id={"tooltip"}>Скоро</Tooltip>;
        var body = (
            <Col xs={4} className={className} onClick={this.handleSelect}>
            <h3 className="item-title">{this.props.title}</h3>
            <Image src={this.props.url} responsive ></Image>
            {form}
            </Col>
        );

        var content = body;

        if (this.props.disabled) {
            content = (
                <OverlayTrigger placement="top" overlay={tooltip}>
                {body}
                </OverlayTrigger>
            );
        }
        return content;
    }
};

module.exports = ProductMixin
