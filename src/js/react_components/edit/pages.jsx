// список страниц макета

var React = require('react');
var Page = require("./page.jsx");

var Pages = React.createClass({
    render: function () {
        var pages = this.props.maquette.pages || [];
        pages = pages.map(function (page, i) {
            return (
                <Page key="i" page={page} index={i} />
            );
        }.bind(this));
        return (
            <div className="maquette-create-right">
                <h1>Pages !</h1>
                {pages}
            </div>
        );
    }
});

module.exports = Pages;
