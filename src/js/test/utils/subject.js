var Subject = function () {
    this.array = [];
};

Subject.prototype.onNext = function (x) {
    this.array.push(x);
};
module.exports = Subject;
