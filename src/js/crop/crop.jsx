var React = require('react');
var Box = require('./box.jsx');

var Crop = React.createClass({
    getDefaultProps: function () {
        return {
            naturalWidth: 0,
            naturalHeight: 0
        };
    },

    render: function () {
        return (
            <div style={{position: "relative", width: this.props.width, height: this.props.height}}>
            <Box/>
            <img {...this.props} />
            </div>
        );
    }
});

module.exports = Crop;
