var _ = require("underscore");

var Store = function (initial_data) {
    this.data = _.extend({
        route: "choice",
        maquette: {},
        files: {
            id: 0
        },
        product_type: ""
    }, initial_data || {});
};

Store.prototype.update = function (data) {
    var maquette = _.pick(data, "maquette");
    _.extend(this.data, _.omit(data, "maquette"));
    if(maquette)
        _.extend(this.data.maquette, maquette.maquette);
};

Store.prototype.retrieve = function () {
    return this.data;
};

Store.prototype.updateFile = function(id, data) {
    _.extend(this.data.files[id], data);
};

module.exports = Store;
