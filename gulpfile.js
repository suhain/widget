var gulp = require('gulp');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var reactify = require('reactify');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var yargs = require('yargs');
var tar = require('gulp-tar');
var gzip = require('gulp-gzip');

gulp.task('sass', function () {
    return gulp.src('./src/css/widget.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dest/css'));
});

gulp.task('cssmin', ['sass'], function () {
    return gulp.src('./dest/css/widget.css')
    .pipe(cssmin())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./dest/css/'));
});

gulp.task('browserify', function () {
    return browserify({
        entries: ['./src/js/main.jsx'],
        transform: [reactify],
    })
    .bundle()
    .pipe(source('widget.js'))
    .pipe(gulp.dest('./dest/js/'));
});

gulp.task('uglify', ['browserify'], function () {
    return gulp.src('./dest/js/widget.js')
    .pipe(uglify({}))
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest('./dest/js/'));
});

gulp.task('default', ['cssmin', 'uglify'], function () {
    gulp.watch(['./src/css/**/*.scss'], ['cssmin']);
    gulp.watch(['./src/js/**/*.js', './src/js/**/*.jsx'], ['uglify']);
});

gulp.copy = function (src, dst) {
    return gulp.src(src, {base: "."}).pipe(gulp.dest(dest));
};

gulp.task('build', ['cssmin', 'uglify'], function () {
//gulp.task('build', function () {
    var argv = yargs.argv;
    var dest = argv.dest || "."; // куда все сложить
    var mode = argv.mode || "dir"; // dir || tar
    var name = argv.name || "frontend"; // название директории или архива

    var stream = gulp.src("dest/**");

    if (mode == "tar") {
        stream = stream.pipe(tar(name + ".tar")).pipe(gzip());
    }

    return stream.pipe(gulp.dest(dest));
});
