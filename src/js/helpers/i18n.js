var _ = require("underscore");

var dictionary = {
    components: {
        edit: {
            upload: {
                bad_download: "Загружено не верное число страниц (%{} из %{})",
                some_download: "Загружено страниц %{} из %{}",
                no_download: "Загрузите %{}"
            },
            upload_file: {
                downloading: "Загрузка",
                delete: "Удалить",
                error: "Похоже, что-то пошло не так... пожалуйста, попробуйте очистить кэш браузера и попробовать ещё раз. Кстати, вы можете сильно помочь нам творить будущее, если напишите об этом случае онлайн-консультанту.",
                msg1: "Проверяем разрешение изображения",
                msg2: "Проверяем цветовое пространство",
                msg3: "Проверяем сумму красок",
                msg4: "Проверяем поля изображения",
                msg5: "Проверяем геометрию",
                msg6: "Проверяем количество узлов в кривых",
                msg7: "Проверяем прозрачности и градиенты",
                msg8: "Производим второстепенные проверки",
                msg9: "Вносим необходимы корректировки",
                msg10: "Делаем превью результата",
                msg11: "Формируем список замечаний и исправлений",
                msg12: "Верификация внесённых изменений"
            },
            page: {
                popoverDelete: "Нажмите, чтобы удалить эту страницу"
            },
            singlepage: "Листовое изделие"
        }
    },
    numerical: {
        page: {
            one: "страница",
            several: "страницы",
            many: "страниц"
        }
    }
};

var find = function(string) {
    var obj = dictionary;
    _.each(string.split("."), function(name) {
        if(obj[name])
            obj = obj[name];
    });
    return _.isString(obj) ? obj : string;
};

var replace_params = function(string, params) {
    var str = string;
    _.each(params, function(_param) {
        str = str.replace("%{}", _param);
    });
    return  str;
};

var numerical = function(name, number) {
    var obj = dictionary.numerical[name];
    if(!obj) return number;
    var cases = [2, 0, 1, 1, 1, 2],
        titles = [obj.one, obj.several, obj.many];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
};

module.exports = function(path) {
    return new function() {
        this.t = function(name) {
            var args = [];
            Array.prototype.push.apply( args, arguments );
            args.shift();
            if(name.indexOf("numerical") == 0)
                return numerical(args[0], args[1]);
            else
                return replace_params(find(path + "." + name), args);
        };
    }
};