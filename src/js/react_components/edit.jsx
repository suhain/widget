// Компонент, отображающий окно загрузки страниц и отображения загруженных

var React = require('react');
var $ = require('jquery');
var i18n = require("../helpers/i18n.js")("components.edit");
var PageHeader = require('react-bootstrap/lib/PageHeader');
var Upload = require('./edit/upload.jsx');
var Pages = require('./edit/pages.jsx');

var Edit = React.createClass({
    getInitialProps: function() {
        return {
            maquette: {
                pages: [],
                format_height: null,
                format_width: null
            },
            product_type: "singlepage"
        };
    },
    render: function () {
        return (
            <div className="col-sm-12">
                <PageHeader>{ i18n.t(this.props.product_type) } ({ this.props.maquette.format_height} x {this.props.maquette.format_width})</PageHeader>
                <div className="row">
                    <div className="col-sm-4">
                        <Upload stream={this.props.stream}
                                files={this.props.files} />
                    </div>
                    <div className="col-sm-8">
                        <Pages stream={this.props.stream}
                               maquette={this.props.maquette} />
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Edit;
