/*
 * Пункт меню. Конкретное изделие.
 */
var React = require('react');
var ProductMixin = require('./product_mixin.jsx');
var constants = require('../../constants.js');
var $ = require('jquery');

var SinglepageForm = React.createClass({
    getDefaultProps: function () {
        return {
            format_options: constants.format_options.singlepage,
            pages_options: constants.pages_options.singlepage
        };
    },
    getInitialState: function () {
        return {
            format: 0,
            pages: 0
        };
    },

    formatChanged: function (event) {
        this.setState({format: event.target.value});
    },

    pagesChanged: function (event) {
        this.setState({pages: event.target.value});
    },

    getFormData: function () {
        var data = {};

        $.extend(data, this.props.format_options[this.state.format].value);
        $.extend(data, this.props.pages_options[this.state.pages].value);
        $.extend(data, {product_type: this.props.type});

        return data;
    },

    submut: function (event) {
        this.props.stream.onNext({
            type: "maquettes:create",
            data: this.getFormData()
        });
    },

    render: function () {
        var format_options = this.props.format_options.map(function (format_option, i) {
            return <option value={i} key={i} >{format_option.label}</option>;
        });

        var pages_options = this.props.pages_options.map(function (pages_option, i) {
            return <option value={i} key={i}>{pages_option.label}</option>;
        });

        return (
            <div className="product-form">

            <label>Печать</label>
            <select onChange={this.pagesChanged} defaultValue={this.state.pages}>{pages_options}</select>
            <label>Формат</label>
            <select onChange={this.formatChanged} defaultValue={this.state.format}>{format_options}</select>

            <button onClick={this.submut}>GO</button>

            </div>
        );
    }
});
var Singlepage = React.createClass({
    mixins: [ProductMixin],
    render_form: function () {
        return <SinglepageForm ref="form" {...this.props} />;
    }
});
module.exports = Singlepage;
