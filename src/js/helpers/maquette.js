var _ = require("underscore");


var setStatus = function(data) {
    _.extend(this, _.omit(data, "pages"));

    _.each(data.pages, createPage.bind(this));

    findErrors.call(this);
};

var findErrors = function() {
    this.errors = [];
    if(this.pages.length != this.pages_count)
        this.errors.push("Не закончена предварительная проверка. Загрузите необходимое количество страниц и дождитесь её окончания.");
    _.each(this.pages, function(_page, i) {
        if(!_page.fixApprove) this.errors.push("Стр. "+ (i+1) +": подтвердите автоматические исправления.");
        if(!_page.right_format) this.errors.push("Стр. "+ (i+1) +": исправьте ошибки формата.");
        if(!_page.right_bleed) this.errors.push("Стр. "+ (i+1) +": исправьте ошибки вылетов.");
    }.bind(this));
    this.isErrors = this.errors.length != 0;
};

var createPage = function(page) {
    if(page.status != 1) return;
    var _page = this.findPage(page.id);
    if(_page)
        _page.setInfo(page);
    else
        this.pages.push(new maquettePage(page));
};

module.exports = function() {
    this.createMaquette = function(data) {
        var Maquette = function() {
            this.is_correct_pages_count = false;
            this.is_ready_to_approve = false;
            this.is_right = false;
            this.pages = [];

            this.size = 0;
            this.name = "noname";
            this.files = [];

            this.is_approve = false; // Был ли заапрувлен макет
            this.errors = []; // Список ошибок макета
            this.isErrors = false; // Найдены ли ошибки в макете

            setStatus.call(this, data);
        };

        return new Maquette();
    };
};