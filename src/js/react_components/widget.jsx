/* @jsx React.DOM */
/*
 * Основной компонент виджета
 */
var React = require('react');

var Grid = require('react-bootstrap/lib/Grid');
var Row = require('react-bootstrap/lib/Row');
var Col = require('react-bootstrap/lib/Col');

var Choice = require('./choice.jsx');
var Edit = require('./edit.jsx');

var products = require('../constants.js').products;

var Widget = React.createClass({
    getInitialState: function () {
        return {
            route: "choice"
        };
    },
    render: function () {
        var childProps = {stream: this.props.stream};
        var content = null;
        switch (this.state.route) {
            case "choice":
                content = <Choice stream={this.props.stream} />;
                break;

            case "edit":
                content = <Edit stream={this.props.stream}
                                maquette={this.state.maquette}
                                files={this.state.files}
                                product_type={this.state.product_type} />;
                break;

        };

        return (
            <Grid>
            <Row className="widget">
            <Col xs={12}>{content}</Col>
            </Row>
            </Grid>
        );
    }
});
module.exports = Widget;
