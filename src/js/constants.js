var PRODUCTS = {
    singlepage: {
        type: "singlepage",
        url: "http://printler.pro/static/img/widget/singlepage.png",
        title: "Листовое изделие",
        disabled: false
    },
    multipage: {
        type: "multipage",
        url: "http://printler.pro/static/img/widget/multipage.png",
        title: "Многостраничное изделие",
        disabled: true
    },
    folded: {
        type: "fold",
        url: "http://printler.pro/static/img/widget/fold.png",
        title: "Фальцуемое изделие",
        disabled: true
    }
};
var FORMAT_OPTIONS = {
    singlepage: [
        {label: "148 × 105 A6", value: {format_height: 148, format_width: 105}},
        {label: "210 × 148 A5", value: {format_height: 210, format_width: 148}},
        {label: "210 × 297 A4", value: {format_height: 210, format_width: 297}}
    ]
};
var PAGES_COUNT_OPTIONS = {
    singlepage: [
        {label: "Односторонняя", value: {pages_count: 1}},
        {label: "Двухстронняя",  value: {pages_count: 2}}
    ]
};
var STATES = ["choice", "upload"];
module.exports = {
    products: PRODUCTS,
    states: STATES,
    format_options: FORMAT_OPTIONS,
    pages_options: PAGES_COUNT_OPTIONS
};
