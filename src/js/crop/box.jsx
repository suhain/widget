var React = require('react');
var ReactDOM = require('react-dom');

var EventMixin = require('react-event-mixin');

var Box = React.createClass({

    mixins: [EventMixin],

    componentDidMount: function () {
        var box = ReactDOM.findDOMNode(this.refs.box);
        this.addEvent(box, 'mousemove', this.onMouseMove);
        this.addEvent(box, 'mouseup', this.onMouseUp);
        this.addEvent(box, 'mousedown', this.onMouseDown);
        this.addEvent(box, 'mouseover', this.onMouseOver);
        this.addEvent(box, 'mouseleave', this.onMouseLeave);
    },

    componentWillUnmount: function () {
        var box = ReactDOM.findDOMNode(this.refs.box);
        this.removeEvent(box, 'mousemove', this.onMouseMove);
        this.removeEvent(box, 'mouseup', this.onMouseUp);
        this.removeEvent(box, 'mousedown', this.onMouseDown);
        this.removeEvent(box, 'mouseover', this.onMouseOver);
        this.removeEvent(box, 'mouseleave', this.onMouseLeave);
    },

    getInitialState: function () {
        return {
            top: 0,
            left: 0,
            clientX: 0,
            clientY: 0,
            move: false
        };
    },

    getDefaultProps: function () {
        return {
            // размеры бокса
            width: 148,
            height: 105
        };
    },

    moveBox: function (clientX, clientY, movedX, movedY) {
        var position = {
            top: this.normalize(this.state.top + movedY, 400 - this.props.height),
            left: this.normalize(this.state.left + movedX, 400 - this.props.width)
        };

        this.setState(Object.assign({
            clientX: clientX,
            clientY: clientY
        }, position));
    },

    normalize: function (n, bound) {
        if (n < 0) {
            return 0;
        } else if (n > bound) {
            return bound;
        } else {
            return n;
        }
    },

    getClientCoordinates: function(e) {
        return e.touches ? {
            clientX: e.touches[0].clientX,
            clientY: e.touches[0].clientY
        } : {
            clientX: e.clientX,
            clientY: e.clientY
        };
    },

    onMouseMove: function (e) {
        e.preventDefault();
        if (this.state.move) {
            var coordinates = this.getClientCoordinates(e);
            var clientX = coordinates.clientX;
            var clientY = coordinates.clientY;
            var movedX = clientX - this.state.clientX;
            var movedY = clientY - this.state.clientY;
            this.moveBox(clientX, clientY, movedX, movedY);
        }
    },

    onMouseDown: function (e) {
        e.preventDefault();
        this.setState({
            move: true,
            clientX: e.clientX,
            clientY: e.clientY
        });
    },

    onMouseUp: function (e) {
        e.preventDefault();
        this.setState({move: false});
    },

    onMouseOver: function (e) {
        e.preventDefault();
    },

    onMouseLeave: function (e) {
        e.preventDefault();
        this.setState({move: false});
    },

    render: function () {
        var style = {
            position: "absolute",
            border: "1px dashed black",
            top: this.state.top,
            left: this.state.left,
            width: this.props.width,
            height: this.props.height,
            cursor: "move"
        };
        return <div style={style} ref="box"></div>;
    },
});

module.exports = Box;
