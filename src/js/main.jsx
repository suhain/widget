"use strict";
var React = require('react');
var ReactDOM = require('react-dom');
var Rx = require('rx');

var View = require('./react_components/widget.jsx');
var Store = require('./store.js');
var dispatch = require('./dispatch.js');
require('./loupe.js');
require('jq-ajax-progress');

window.React = React;

var stream = new Rx.Subject();
var store = new Store();
var view = ReactDOM.render(
    <View stream={stream} />,
        document.getElementById('content')
    //document.body
);
dispatch(stream, view, store);
