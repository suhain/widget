var Rx = require('rx');
var $ = require('jquery');
var SockJS = require('sockjs-client');

var sock_url = "http://localhost:8000/";

function bindSocketNotifications(stream) {
    var sock = new SockJS(sock_url);
    console.debug("sock initialized", sock);

    sock.onmessage = function (msg) {
        console.debug("message recieved", message);
        stream.onNext({type: "ws", data: JSON.parse(msg)});
    };
}

module.exports = function dispatch(stream, view, store) {
    bindSocketNotifications(stream);

    var createStream = stream.filter(function (payload) {
        return payload.type == "maquettes:create";
    }).flatMap(function (payload) {
        return Rx.Observable.create(function (observer) {
            // $.post("/maquettes_new/api/v1/maquettescreate/", store.retrieve())
            $.ajax("/src/js/stubs/create_maquette.json")
                .then(function (response) {
                    observer.onNext({
                        status: "done",
                        data: {
                            maquette: response,
                            route: "edit",
                            product_type: payload.data.product_type
                        }
                    });
                }, function (error) {
                    observer.onNext({status: "fail", error: {
                        errorMessage: "Не удалось создать макет. Попробуйте позже."}
                    });
                });
        });
    });

    var wsStream = stream.filter(function (payload) {
        return payload.type == "ws";
    }).map(function (payload) {
        return payload.data;
    }).share();

    var checkedWsStream = wsStream.filter(function (sock_data) {
        return sock_data.event == "checked";
    });

    var croppedWsStream = wsStream.filter(function (sock_data) {
        return sock_data.event == "cropped";
    });

    var maxpagesWsStream = wsStream.filter(function (sock_data) {
        return sock_data.event == "maxpages";
    });

    var errorWsStream = wsStream.filter(function (sock_data) {
        return sock_data.event == "error";
    });

    var detailsStream = stream.filter(function (payload) {
        return payload.type == "maquettes:details";
    }).flatMap(function (payload) {
        return Rx.Observable.create(function (observer) {
            $.ajax("/maquettes_new/api/v1/maquettes/" + store.retrieve().id + "/")
            .then(function(data) {
                observer.onNext({status: "done", data: data});
            }, function () {
                observer.onNext({status: "fail", error: {errorMessage: "Не удалось получить данные о макете."}});
            });
        });
    });

    var uploadStream = stream.filter(function (payload) {
        return payload.type == "maquettes:upload";
    }).flatMap(function (payload) {
        return Rx.Observable.create(function (observer) {
            //$.post("upload_url", $(payload.form).serialize())
            // Создаём новый файл с новым ID
            var input = payload.data.children[0],
                files = {},
                id = store.data.files.id;
            files[id] = {
                progress:        0,
                name:            input.files[0].name,
                maquettefile_id: undefined
            };
            files.id = id + 1;
            store.update({ files: files });
            view.setState(store.data);

            $.ajax({
                url: "/src/js/stubs/file1_download.json",
                success: function(data) {
                    store.updateFile(id, { maquettefile_id: data.maquettefile_id });
                    view.setState(store.data);
                },
                error: function() {
                    observer.onNext({status: "fail", error: {errorMessage: "Че то не загружается."}});
                }
            }).uploadProgress(function(e) {
                if(e.lengthComputable) {
                    store.updateFile(id, { progress: (e.loaded * 100) / e.total });
                    view.setState(store.data);
                }
            });
        });
    });

    return Rx.Observable.merge(
        createStream,
        uploadStream
    ).subscribe(function (r) {
        if (r.status == "done") {
            store.update(r.data);
            view.setState(store.data);
        } else {
            view.setState(r.error);
        }
    });
};
