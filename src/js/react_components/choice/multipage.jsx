/*
 * Пункт меню. Конкретное изделие.
 */
var React = require('react');
var ProductMixin = require('./product_mixin.jsx');
var constants = require('../../constants.js');

var MultipageFormMixin = {
    handleClick: function () {
        this.props.clickCallback(this.props.type);
    },
    render_form: function () {
        return null;
    }
};
var Multipage = React.createClass({
    mixins: [ProductMixin, MultipageFormMixin]
});

module.exports = Multipage;
