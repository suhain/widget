var React = require('react');
var ReactDOM = require('react-dom');
var $ = require('jquery');

var Loupe = React.createClass({
    componentDidMount: function () {
        $(ReactDOM.findDOMNode(this.refs.loupe)).find("img:first").loupe(this.props);
    },
    render: function () {
        return <div ref="loupe">{this.props.children}</div>;
    }
});
module.exports = Loupe;
