// загрузка макетов

var React = require('react');
var _ = require('underscore');
var i18n = require("../../helpers/i18n.js")("components.edit.upload");
var UploadFile = require("./upload_file.jsx");

var UploadMaquette = React.createClass({
    render: function () {
        var queue = _.omit(this.props.files, "id") || [];
        queue = _.map(queue, function (file, i) {
            return (
                <UploadFile key="i" file={file} removeFile={this.removeFile.bind(this, i)} />
            );
        }.bind(this));
        return (
            <div>
                <form ref="form" encType="multipart/form-data">
                    <input ref="filesInput" type="file" name="files[]" multiple="false" onChange={this.addFiles} />
                </form>
                { queue }
            </div>
        );
    },
    addFiles: function() {
        this.props.stream.onNext({
            type: "maquettes:upload",
            data: this.refs.form
        });
    },
    removeFile: function(i) {
        var files = this.state.files;
        files.splice(i,1);
        this.setState({ files: files });
    }
});

module.exports = UploadMaquette;
