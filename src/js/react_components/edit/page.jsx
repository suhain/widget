// список страниц макета

var React = require('react');
var OverlayTrigger = require('react-bootstrap').OverlayTrigger;
var Popover = require('react-bootstrap').Popover;
var i18n = require("../../helpers/i18n.js")("components.edit.page");

var Page = React.createClass({
    render: function () {
        return (
            <h1>Page!</h1>
            // <div className="row maquette-page" onClick={this.openPage}>
            //     <div className="col-sm-10 maquette-page-container">
            //         <OverlayTrigger trigger="hover" placement="bottom" overlay={<Popover>{i18n.t("popoverDelete")}</Popover>}>
            //             <a className="btn default maquette-page-delete"
            //                href="#" onClick={this.deletePage}>x</a>
            //         </OverlayTrigger>
            //         <div className="maquette-page-messages-img">
            //             <img src="{this.props.page.small_preview}"
            //                  style="
            //                     -webkit-transform: rotate({[page.angle(maquette.inverted_format)]}deg);
            //                     -moz-transform: rotate({[page.angle(maquette.inverted_format)]}deg);
            //                     -o-transform: rotate({[page.angle(maquette.inverted_format)]}deg);
            //                     -ms-transform: rotate({[page.angle(maquette.inverted_format)]}deg);
            //                     transform: rotate({[page.angle(maquette.inverted_format)]}deg);
            //                  ">
            //             <p className="page-name">Страница { this.props.index + 1 }</p>
            //         </div>
            //         <div className="maquette-page-messages">
            //             <p ng-hide="page.fixApprove" pulsate="true">
            //                 <i className="fa fa-question-circle text-warning"></i>Есть замечания и исправления, нажмите, чтобы утвердить
            //             </p>
            //             <p ng-show="page.fixApprove && page.needApprove">
            //                 <i className="fa fa-check-circle text-success"></i>Замечания и исправления утверждены
            //             </p>
            //             <p ng-show="page.right_format && !page.was_stretched">
            //                 <i className="fa fa-check-circle text-success"></i>Размер изображения в порядке
            //             </p>
            //             <p ng-show="page.right_format && page.was_stretched && page.fixApprove">
            //                 <i className="fa fa-check-circle text-success"></i>Изменение размера изображения утверждено
            //             </p>
            //             <p ng-show="page.right_format && page.was_stretched && !page.fixApprove">
            //                 <i className="fa fa-question-circle text-warning"></i>Изображение растянуто под указанный размер
            //             </p>
            //             <p ng-show="!page.right_bleed" pulsate="true">
            //                 <i className="fa fa-exclamation-circle text-danger"></i>Найдены элементы близкие к линии реза
            //             </p>
            //             <p ng-show="page.right_bleed">
            //                 <i className="fa fa-check-circle text-success"></i>Поля изображения в порядке
            //             </p>
            //         </div>
            //     </div>
            //     <div className="col-sm-2">
            //         <a ng-hide="(page.is_right && page.fixApprove) || page.status > 1" className="btn btn-primary"
            //            ng-click="statistics.action('FIX IT')"
            //            ui-sref="maquettes.edit.page.results({id: maquette.id, page_id: page.id})">Исправить</a>
            //     </div>
            // </div>
        );
    },
    openPage: function() {
        // statistics.action('FIELD'); goto(page);
    },
    deletePage: function() {
        // delete page
    }
});

module.exports = Page;
