/*
 * Меню выбора изделия
 */
var React = require('react');
var PageHeader = require('react-bootstrap/lib/PageHeader');
var Singlepage = require('./choice/singlepage.jsx');
var Multipage = require('./choice/multipage.jsx');
var Folded = require('./choice/folded.jsx');

var Choice = React.createClass({
    getInitialState: function () {
        return {
            productType: null
        };
    },
    selectCallback: function (product) {
        this.setState({ productType: product });
    },
    render: function () {
        var products = require('./../constants.js').products;
        return (
            <div className="col-sm-12">
                <PageHeader>Выберите изделие</PageHeader>
                <div className="products-menu">
                    <Singlepage
                        {...products.singlepage}
                        stream={this.props.stream}
                        selectedProductType={this.state.productType}
                        selectCallback={this.selectCallback} />
                    <Multipage
                        {...products.multipage}
                        stream={this.props.stream}
                        selectedProductType={this.state.productType}
                        selectCallback={this.selectCallback} />
                    <Folded
                        {...products.folded}
                        stream={this.props.stream}
                        selectedProductType={this.state.productType}
                        selectCallback={this.selectCallback} />
                </div>
            </div>
        );
    }
});
module.exports = Choice;
