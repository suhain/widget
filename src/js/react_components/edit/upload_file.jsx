// загрузка файла

var React = require('react');
var $ = require('jquery');
var ProgressBar = require('react-bootstrap').ProgressBar;
var i18n = require("../../helpers/i18n.js")("components.edit.upload_file");

var msgs = [
    6000,
    19000,
    32000,
    39000,
    46000,
    55000,
    64000,
    71000,
    80000,
    85000,
    91000,
    95000
];

var UploadFile = React.createClass({
    getInitialState: function() {
        return {
            progress: 0,
            isError: false,
            message: "downloading",
            intervalID: undefined
        };
    },
    render: function () {
        if(this.state.isError)
            return (
                <div>
                    <p>{i18n.t("error")} <a href="#" style={{color:"red"}} onClick={this.props.removeFile}><b>{i18n.t("delete")}</b></a></p>
                </div>
            );
        else
            return (
                <div className="printler-progress-bar">
                    <p>{this.props.file.name} ({i18n.t(this.props.file.message)})</p>
                    <ProgressBar active now={this.props.file.progress} />
                </div>
            );
    },
    callMsgs: function(i) {
        var T = 95000;
        var step = 100;
        var time = 0;
        var intervalID = setInterval(function() {
            if(time > T) {
                clearInterval(this.state.intervalID);
                this.setState({
                    isError: true,
                    intervalID: undefined
                });
            } else {
                var i = 0;
                time += step;
                while(time > msgs[i]) { i++; };
                if(i>11) i=11; // лалала
                this.setState({
                    message: "msg"+(i+1),
                    progress: time*100/T
                });
            }
        }.bind(this), step);
        this.setState({intervalID: intervalID});
    },
});

module.exports = UploadFile;
