require('./utils/dom')('<html><body></body></html>');
var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var Singlepage = require('../react_components/choice/singlepage.jsx');
var assert = require('assert');
var Subject = require('./utils/subject.js');


describe('Singlepage Product component', function(){

    var product = require('../constants.js').products.singlepage;
    var selectCallback = function (product) {
        selected = product;
        return product;
    };
    var element, selected;

    beforeEach(function () {
        selected = null;
        subject = new Subject();
        element = TestUtils.renderIntoDocument(
            <Singlepage {...product} stream={subject} selectedProductType={selected} selectCallback={selectCallback} />
        );
    });

    it('is DOM Component', function () {
        assert(TestUtils.isDOMComponent(ReactDOM.findDOMNode(element)));
    });

    it('is React Class', function () {
        assert(TestUtils.isCompositeComponent(element));
    });

    it('click should update parent "selected" state param', function () {
        var content = TestUtils.findRenderedDOMComponentWithTag(
            element, 'div'
        );
        TestUtils.Simulate.click(content);
        // should update selected
        assert(selected == product.type);
    });

    it('should send form on submit', function () {
        var content = TestUtils.findRenderedDOMComponentWithTag(
            element, 'div'
        );
        TestUtils.Simulate.click(content);
        console.log(element.refs);
    });
});
