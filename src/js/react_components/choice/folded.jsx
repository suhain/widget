/*
 * Пункт меню. Конкретное изделие.
 */
var React = require('react');
var ProductMixin = require('./product_mixin.jsx');
var constants = require('../../constants.js');

var FoldedFormMixin = {
    handleClick: function () {
        this.props.clickCallback(this.props.type);
    },
    render_form: function () {
        return null;
    }
};

var Folded = React.createClass({
    mixins: [ProductMixin, FoldedFormMixin]
});

module.exports = Folded;
